document.addEventListener('DOMContentLoaded', () => {
  const themeBtn = document.getElementById('theme-color');
  const body = document.body;

  if (localStorage.getItem('theme') === 'dark') { 
    body.classList.add('dark-theme'); 
  }

  themeBtn.addEventListener('click', () => {
    if (body.classList.contains('dark-theme')) {
      body.classList.remove('dark-theme');
      localStorage.setItem('theme', 'light');
    } else {
      body.classList.add('dark-theme');
      localStorage.setItem('theme', 'dark');
    }
  });
})
